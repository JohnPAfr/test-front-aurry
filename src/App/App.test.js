import { render, screen } from '@testing-library/react';
import App from './App';

test('renders the header', () => {
  render(<App />);
  const headerElement = screen.getByRole('banner');
  expect(headerElement).toBeInTheDocument();
});

test('renders a footer', () => {
  render(<App />);
  const footerElement = screen.getByRole('contentinfo')
  expect(footerElement).toBeInTheDocument();
})

test('renders multiple links', () => {
  render(<App />);
  const links = screen.getAllByRole('link')
  links.map((link) => expect(link).toBeInTheDocument())
})