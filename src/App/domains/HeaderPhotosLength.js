import React from "react";
import PropTypes from 'prop-types'

const HeaderPhotosLength = ({ length }) => {
  return <div>{length}</div>;
};

HeaderPhotosLength.propTypes = {
  length: PropTypes.number
}

export default HeaderPhotosLength
