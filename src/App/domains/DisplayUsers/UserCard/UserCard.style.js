import styled from 'styled-components'

export const Card = styled.div`
  position: relative;
  width: 100%;
  text-align: center;
  box-shadow: rgb(51 173 115 / 20%) 2px 2px 15px;

  &::before {
    display: block;
    content: "";
    width: 100%;
    height: 12px;
    border-radius: 12px 12px 0px 0px;
    background-image: linear-gradient(to left, rgb(119, 209, 166), rgb(164, 219, 149));
  }

  h4 {
    color: #33ad73
  }
`

export const UserName = styled.h2`
  color: #33ad73;
`

export const UserUsername = styled.h3`
  color: #00a99d;
`

export const UserWebsite = styled.a`
  position: relative;
  padding-bottom: 5px;
  font-weight: 600;
  color: #a26ae1;
  text-decoration: none;

  &::after {
    content: "";
    position: absolute;
    width: 100%;
    height: 2px;
    bottom: 0;
    left: 0;

    background-color: #a26ae1;
    transition: 0.3s all ease-in-out;
    transform: scaleX(0.2);
    transform-origin: 0 100%;
  }
  &:hover::after {
    transform: scaleX(1);
  }
`

export const UserFlex = styled.p`
  display: flex;
  align-items: center;
  justify-content: center;

  svg {
    margin-right: 10px;
    fill: #00a99d
  }
`