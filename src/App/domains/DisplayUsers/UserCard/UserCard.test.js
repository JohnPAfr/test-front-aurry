import { render } from '@testing-library/react';

import UserCard from './UserCard'

const user = {
  "id": 1,
  "name": "Leanne Graham",
  "username": "Bret",
  "email": "Sincere@april.biz",
  "address": {
  "street": "Kulas Light",
  "suite": "Apt. 556",
  "city": "Gwenborough",
  "zipcode": "92998-3874",
  "geo": {
  "lat": "-37.3159",
  "lng": "81.1496"
  }
  },
  "phone": "1-770-736-8031 x56442",
  "website": "hildegard.org",
  "company": {
  "name": "Romaguera-Crona",
  "catchPhrase": "Multi-layered client-server neural-net",
  "bs": "harness real-time e-markets"
  }
}

test("should render a titles", () => {
  const { getByText } = render(<UserCard user={user} />);


  expect(getByText("ADDRESS")).toBeInTheDocument();
  expect(getByText("COMPANY")).toBeInTheDocument();
})

test("should render names", () => {
  const { getByText } = render(<UserCard user={user} />);


  expect(getByText("Leanne Graham")).toBeInTheDocument();
  expect(getByText("aka Bret")).toBeInTheDocument();
})