import React from 'react'
import { Card, UserFlex, UserName, UserUsername, UserWebsite } from './UserCard.style'

const UserCard = ({ user }) => {
  return (
    <Card>
      <UserName>{user.name}</UserName>
      <UserUsername>aka {user.username}</UserUsername>
      <UserFlex> 
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-envelope-fill" viewBox="0 0 16 16">
          <path d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555zM0 4.697v7.104l5.803-3.558L0 4.697zM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757zm3.436-.586L16 11.801V4.697l-5.803 3.546z"/>
        </svg>
        {user.email}
      </UserFlex>

        <h4>ADDRESS</h4>
        <p>{user.address.street}, {user.address.suite}</p>
        <p>{user.address.city}, {user.address.zipcode}</p>
        <UserFlex>
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-geo-alt-fill" viewBox="0 0 16 16">
            <path d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 1 0-6 3 3 0 0 1 0 6z"/>
          </svg>
          lat: {user.address.geo.lat}, lng: {user.address.geo.lng}
        </UserFlex>

      <UserFlex>
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" className="bi bi-phone-fill" viewBox="0 0 16 16">
          <path d="M3 2a2 2 0 0 1 2-2h6a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V2zm6 11a1 1 0 1 0-2 0 1 1 0 0 0 2 0z"/>
        </svg> 
        {user.phone}
      </UserFlex>

      <UserWebsite href="https://paygreen.io/" target="_blank" rel="noreferrer">{user.website}</UserWebsite>
      
      <h4>COMPANY</h4>
      <p>{user.company.name}</p>
      <p>{user.company.catchPhrase}</p>
      <p>{user.company.bs}</p>

    </Card>
  )
}

export default UserCard
