import React, { useEffect, useState } from 'react'
import { UsersGrid } from './DisplayUsers.style'
import UserCard from './UserCard'

const Users = () => {
  const [users, setUsers] = useState(null)

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/users", )
      .then((data) => {
        if (data.ok)
          return data.json()
      })
      .then((data) => setUsers(data))
  }, [])
  return (
    <UsersGrid>
      {users?.map((user) => {
        return <UserCard user={user} key={user.id}/>
      })}
    </UsersGrid>
  )
}

export default Users
