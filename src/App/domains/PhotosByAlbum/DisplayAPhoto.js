import React, { useEffect, useState } from "react";

const DisplayAPhoto = () => {
  // modifyied state to get the title from the request to use it as alt attr
  const [photoData, setPhotoData] = useState({
    url: null,
    alt: null
  });

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/photos/1")
    .then((data) => data.json())
    .then((data) => {
      setPhotoData(prev => {
        return {
          ...prev,
          url: data.url,
          alt: data.title
        }
      });
    });
  }, [])

  // adding alt attr to the img
  return <div>{photoData ? <img width="50" src={photoData.url} alt={photoData.alt}/> : null}</div>;
};

export default DisplayAPhoto