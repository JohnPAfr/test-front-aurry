import React, { useEffect, useState } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

// Display photos which a title length <= to the limit in parameter

const Style = styled.div`
  color: black;
`;

const ImageFlex = styled.div`
  display: flex;
  flex-wrap: wrap;

  margin-bottom: 10px;

  img {
    width: 150px;
    height: 150px;
  }
`

const PhotosByAlbum = ({ limit }) => {
  const [array, setAlbums] = useState([]);
  const displayLimitSize = limit;

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/photos")
      .then((data) => data.json())
      .then((data) => {
        // var to const
        const t0 = performance.now();

        const datachanged = data
          .sort((a, b) => {
            return a.title.length > b.title.length;
          })
          .map((value) => {
            value.title = value.title.toUpperCase();
            value.titleNbLetters = value.title.length;
            return value;
          });

        const dataSorted = datachanged.filter(
          (v) => v.title.length <= displayLimitSize
        );

        const albums = [];
        dataSorted.forEach((data) => {
          if (albums[data.albumId] && albums[data.albumId].length >= 0) {
            albums[data.albumId].push(data);
          } else {
            albums[data.albumId] = [data];
          }
        });
        setAlbums(albums);
        // var to const
        const t1 = performance.now();
        console.log("Call to PhotoByAlbums took " + (t1 - t0) + " milliseconds.");
      });
      // adding the limit to the dependencies to rerender when the limit changes
  }, [displayLimitSize]);

  return (
    <Style>
      {array.map((value, key) => {
        return (
          <ImageFlex key={key}>
            {value.map((photo) => {
              return <img src={photo.url} alt={photo.title} key={photo.id}/>
            })}
          </ImageFlex>
        );
      })}
    </Style>
  );
};

PhotosByAlbum.propTypes = {
  limit: PropTypes.number
}

export default PhotosByAlbum;