import { fireEvent, render, screen } from '@testing-library/react';
import Home from './Home';

test('renders the limit select', () => {
  render(<Home />);
  let limit = screen.getByText("10");
  expect(limit).toBeInTheDocument();
  fireEvent.click(screen.getByText('25'));
  expect(screen.getByText('25')).toBeInTheDocument();
});