import React from "react";
import PropTypes from "prop-types";

import DisplayLimit from "./PhotosByAlbum/DisplayLimit";


const Home = ({ limit, setLimit }) => {
  return (
    <div>
      <DisplayLimit setLimit={setLimit} limit={limit} />
    </div>
  );
};

Home.propTypes = {
  limit: PropTypes.number,
  setLimit: PropTypes.func
}

export default Home