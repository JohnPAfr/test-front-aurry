import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { useEffect, useState } from "react";
import styled from "styled-components";

import HeaderPhotosLength from "./domains/HeaderPhotosLength";
import Home from "./domains/Home";
import DisplayLimit from "./domains/PhotosByAlbum/DisplayLimit";
import DisplayAPhoto from "./domains/PhotosByAlbum/DisplayAPhoto";
import PhotosByAlbum from "./domains/PhotosByAlbum/PhotosByAlbum";

import DisplayUsers from './domains/DisplayUsers';

const Main = styled.div`
  display: flex;
  flex-direction: column;
  height: 100vh;

  color: black;
`;

const Header = styled.header`
  height: 150px;
  box-sizing: border-box;
  padding: 20px;
  background-color: #33ad73;
  display: flex;
  align-items: center;
  justify-content: space-between;
  color: white;
  a {
    position: relative;
    padding-bottom: 10px;
    font-weight: 600;
    color: white;
    text-transform: uppercase;
    text-decoration: none;
  }
  a::after {
    content: "";
    position: absolute;
    width: 100%;
    height: 2px;

    bottom: 0;
    left: 0;

    background-color: #d5f3e5;
    transition: 0.3s all ease-in-out;
    transform: scaleX(0);
    transform-origin: 0 100%;
  }
  a:hover::after {
    transform: scaleX(1);
  }
  & > a {
    margin-right: 20px;
  }
`;

const Flex = styled.div`
  display: flex;
  align-items: center;
  & > * {
    margin: 10px;
  }
`;

const Content = styled.div`
  padding: 20px;
  flex: 1 0 auto;
`;

const Footer = styled.footer`
  margin-top: auto;
  padding: 24px;

  color: white;
  background-color: #33ad73;
`

function App() {
  const [photos, setPhotos] = useState([]);
  const [limit, setLimit] = useState(25);

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/photos")
      .then((data) => data.json())
      .then((data) => {
        setPhotos(data);
      });
  }, []);

  return (
      <Main>
        <Router>
          <Header>
            <Flex>
              <Link to="/">Home</Link>
              <Link to="/photos-by-album">Photos by Album</Link>
              <Link to="/users">Users</Link>
            </Flex>
            <Flex>
              <DisplayAPhoto />
              <HeaderPhotosLength length={photos.length} />
              <DisplayLimit setLimit={setLimit} limit={limit} />
            </Flex>
          </Header>

          <Content>
            <Switch>
              <Route path="/photos-by-album">
                <PhotosByAlbum limit={limit} />
              </Route>
              <Route path="/users">
                <DisplayUsers />
              </Route>
              <Route path="/">
                <Home setLimit={setLimit} limit={limit} />
              </Route>
            </Switch>
          </Content>

          <Footer>Ceci est le footer</Footer>
        </Router>
      </Main>
  );
}

export default App;
